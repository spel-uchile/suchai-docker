#!/bin/bash
cd SUCHAI-Flight-Software
git pull
git checkout feature/framework
test=$1
if [ "$test" = "test_unit" ]
then
    sh run_tests.sh -b 0 -u 1 -c 0 -d 0 -l 0 -s 0 -t 0 -f 0
elif [ "$test" = "test_int" ]
then
    sh run_tests.sh -b 0 -u 0 -c 1 -d 0 -l 1 -s 1 -t 1 -f 0
fi

status=$?
echo "The exit status was $status"
cd -
exit $status
