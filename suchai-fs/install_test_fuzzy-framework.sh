#!/bin/bash
cd SUCHAI-Flight-Software
git pull
git checkout feature/framework
version=$(cmake -version)
echo $version

cmake -B build -DAPP=simple -DSCH_FP_ENABLED=0 -DSCH_HK_ENABLED=0 -DSCH_CSP_BUFFERS=2000000 
cmake --build build
cd ../SUCHAI-FS-Fuzzy-Testing
strategy=$(($RANDOM%3 +1))
echo "STRATEGY: " $strategy

if [ $strategy = 1 ]
then
    python3 run_experiment.py --iterations 15 --commands_number 12 --strategy 1
elif [ $strategy = 2 ]
then
    python3 run_experiment.py --iterations 15 --commands_number 12 --strategy 2
else
    python3 run_experiment.py --iterations 15 --commands_number 18  --strategy 3
fi

status=$?
cd -
echo "The exit status was $status"
exit $status
    
