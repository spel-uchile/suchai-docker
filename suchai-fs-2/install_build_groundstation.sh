#!/bin/bash
cd suchai-2-software
git pull
git checkout framework
cd apps/groundstation/src/drivers/
sh install.sh --ssh
cd -
rm -rf build-gnd
sh build_groundstation.sh 0 # Build without payloads commands
status=$?
echo "The exit status was $status"
exit $status
