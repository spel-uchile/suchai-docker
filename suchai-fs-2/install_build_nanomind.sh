#!/bin/bash
cd suchai-2-software
git pull
git checkout framework
cd apps/plantsat/src/drivers/
sh install.sh --ssh
cd -
sh build_plantsat.sh
status=$?
echo "The exit status was $status"
exit $status
